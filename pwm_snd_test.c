#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "hardware/dma.h"
#include "hardware/pll.h"
#include "hardware/clocks.h"

//#include "pwm_retrosound.h"

/*uint8_t track[] = {
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(D, 3),
			MAKE_NOTE(D, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(F, 3),
			MAKE_NOTE(F, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			PAUSE,
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			PAUSE,
			PAUSE
};*/

//uint trackLen = 80;

uint16_t samples[16] __attribute__((aligned(32))) = {512, 707, 873, 984, 1023, 984, 873, 707, 512, 316, 150, 39, 0, 39, 150, 316};


int main() {
	uint dmaChannelId, dmaTimer, pwmSlice;
	dma_channel_config dmaChannelConf;

	pwmSlice = 0;

	set_sys_clock_khz(141600, true);

	stdio_init_all();

	gpio_init(PICO_DEFAULT_LED_PIN);
	gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);

	gpio_put(PICO_DEFAULT_LED_PIN, 1);

	sleep_ms(2000);

	printf("started\n");
	
	gpio_init(16);
	gpio_set_dir(16, GPIO_OUT);
	gpio_set_function(16, GPIO_FUNC_PWM);

	// this gives a 46.09375 kHz square wave
	pwm_set_clkdiv_int_frac(pwmSlice, 3, 0);
	pwm_set_wrap(pwmSlice, 1023);
	pwm_set_counter(pwmSlice, 0);

	dmaChannelId = dma_claim_unused_channel(true);
	// now create the DMA configuration.
	dmaChannelConf = dma_channel_get_default_config(dmaChannelId);
	// we need 32 bit transfers
	channel_config_set_transfer_data_size(&dmaChannelConf, DMA_SIZE_16);
	// read address needs to be incremented as we're reading from a buffer
	channel_config_set_read_increment(&dmaChannelConf, true);
	// write address must be constant when writing to PIO FIFO
	channel_config_set_write_increment(&dmaChannelConf, false);
	// set read ring with 32 bytes (16 samples @ 2 Bytes) -> 2^5 = 32
	channel_config_set_ring(&dmaChannelConf, false, 5);
	
	// get a DMA pacing timer
	dmaTimer = dma_claim_unused_timer(true);

	// set channel B compare value (for testing only)
	pwm_set_chan_level(pwmSlice, 1, 60);

	// set the timer's divider
	// 440 Hz, A4
//	dma_timer_set_fraction(dmaTimer, 3, 60341);
	// 220 Hz, A3
	dma_timer_set_fraction(dmaTimer, 1, 40227);

	// we need to set the proper data request source (PIO can do that!)
	channel_config_set_dreq(&dmaChannelConf, dma_get_timer_dreq(dmaTimer));//pwm_get_dreq(pwmSlice));

	// configure the DMA channel; write address can be set up statically, write count varies
	dma_channel_configure(
		dmaChannelId,
		&dmaChannelConf,
		&pwm_hw->slice[pwmSlice].cc,	// write address (counter compare register of selected PWM slice (A))
		samples,		// read from samples buffer, wrapping after 32 bytes
		35200,			// 16 samples x 440 Hz x 5 seconds
		true			// start immediately
	);

	pwm_set_enabled(pwmSlice, true);

	while(dma_channel_is_busy(dmaChannelId)) {}

	printf("content of PWM CC register: %08x\n", pwm_hw->slice[pwmSlice].cc);

	while(1) {}

	return 0;
}