# rppico-pwm-sound

Software synthesizer and tracker using a RP2040 PWM module.

All you need to test are:
- a Raspberry Pi Pico board
- two capacitors (1 and 10 uF)
- one of those cheapo plastic speakers (mine is an 8 Ohm one)
- probably a bread board and some wires.

Connect GPIO 16 of the RP2040 to both capacitors' positive terminal. The negative terminal of the 10 uF cap connects to the + terminal of the speaker. The negative terminal of the 1 uF cap connects to ground as does the speaker's - terminal.

Code contained:
pwm_snd_test.c: very simple test program creating a static tone (220 Hz by default). Used to explore the possibilities for generating sound using PWM.
pwm_tracker_test.c: test program for the synthesizer, mixer and tracker code in pwm_retrosound.c. Plays a small tune with two voices.

pwm_retrosound.c: synthesizer, mixer and tracker for generating sound using PWM. Designed to use short sound samples which are frequency modulated by the mixer according to tone data provided by the tracker. Contains a 48 kHz software mixer.
pwm_retrosound.h: header file to include if you want to use any of the functions provided.
pwm_retrosound_consts.h: contains some constants just used internally by the synthesizer, tracker and mixer code like sound channel data structure, fixed samples and constants for resampling.

CMakeLists.txt: required by CMake, the build system used by the Raspberry Pi Pico SDK. Tells the build system what to do with the files.

Build instructions:
First clone this repository:
git clone https://codeberg.org/chipfire/rppico-pwm-sound.git

Next, download the RP2040 SDK (follow instructions in the getting started PDF, https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf).

Then create the build directory (this will avoid messing up the code directory) inside the cloned repository and change to it:
mkdir build
cd build

Make sure you have set the environment variable PICO_SDK_PATH and call CMake:
cmake ..

If you run into trouble, pass it the PICO_SDK_PATH variable (I always had to do this, something's messed up):
cmake -DPICO_SDK_PATH=/path/to/pico/sdk ..

Now all you have to do is call Make:
make

That should be all.
