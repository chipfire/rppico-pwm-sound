#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "hardware/dma.h"
#include "hardware/timer.h"
#include "hardware/irq.h"

#include "pico/stdlib.h"

#include <stdio.h>

#include "pwm_retrosound.h"
#include "pwm_retrosound_consts.h"

#define NOTE_OCTAVE(x)		(((x) & 0x70) >> 4)
#define NOTE_TONE(x)		((x) & 0xf)

static uint sndBpm = 0;
static enum Subbeat subbeat = FULL;
static bool running = false, initialized = false;

static channel_t channels[N_SOUND_CHANNELS];

// this is used as a 24.8 fixed point value
static uint trackPtrUpdateDeltaSamples;
static uint trackPtrCounter, samplesProduced;

uint16_t outputBuffers[2][32] __attribute__((aligned(32)));
uint rdBuffer, wrBuffer;

// DMA & PWM resources
uint dmaChannel, dmaTimer, pwmSlice;


void doMixing() {
	int16_t mixingBuffer, offset, interpolation[2], sValues[2];
	uint8_t note;
	bool incrementTrackPtr;

	for(uint sample = 0; sample < 32; ++sample) {
		mixingBuffer = pwmCenter;
		incrementTrackPtr = false;

		// check whether track index has to be incremented.
		if(samplesProduced > (trackPtrCounter >> 8)) {
			trackPtrCounter -= (samplesProduced << 8);
			trackPtrCounter += trackPtrUpdateDeltaSamples;
			samplesProduced = 0;

			incrementTrackPtr = true;
		}

		for(uint c = 0; c < N_SOUND_CHANNELS; ++c) {
			if((channels[c].cs == PLAYING) && (channels[c].trackLength > 0)) {
				if(incrementTrackPtr) {
					++channels[c].trackPtr;

					if(channels[c].trackPtr >= channels[c].trackLength)
						channels[c].trackPtr = 0;
				}

				note = channels[c].track[channels[c].trackPtr];

				if(note != PAUSE) {
					interpolation[0] = (channels[c].sampleOffset & 0xffff) >> 12;
					interpolation[1] = (0x10000 - (channels[c].sampleOffset & 0xffff)) >> 12;

					offset = channels[c].sampleOffset >> 16;

					sValues[0] = (channels[c].wavetable[offset & 0xf] * channels[c].volume) >> 4;
					sValues[1] = (channels[c].wavetable[(offset + 1) & 0xf] * channels[c].volume) >> 4;

					// linear interpolation between two wavetable items
					mixingBuffer += ((sValues[0] * interpolation[0]) >> 4) + ((sValues[1] * interpolation[1]) >> 4);
				}

				channels[c].sampleOffset += toneStepPerSample[note];
			}
		}

		outputBuffers[wrBuffer & 1][sample] = (uint16_t)mixingBuffer;
		++samplesProduced;
	}

	++wrBuffer;
}

void nextBufferInterrupt() {
	// first clear the interrupt flag; for some reason there's no function for that.
	dma_hw->ints0 = 1u << dmaChannel;

	// start DMA transfer of the next buffer
	dma_channel_set_read_addr(dmaChannel, outputBuffers[rdBuffer & 1], true);

	++rdBuffer;

	doMixing();
}

static void startSound() {
	if(!running) {
		trackPtrCounter = 0;
		samplesProduced = 0;
		rdBuffer = 0;
		wrBuffer = 0;

		// fill the first buffer by calling doMixing(), afterwards it will be
		// called when the DMA triggers an interrupt.
		doMixing();

		// start the first DMA transfer which will trigger the second buffer to be filled
		nextBufferInterrupt();

		running = true;
	}
}

void retrosound_init(uint timerAlarm, uint pwmGpio) {
	dma_channel_config dmaChannelConf;

	if((pwmGpio >= NUM_BANK0_GPIOS) || (pwmGpio + 8 >= NUM_BANK0_GPIOS))
		return;

	for(int c = 0; c < N_SOUND_CHANNELS; ++c) {
		channels[c].wf = SINE;
		channels[c].wavetable = sine;
		channels[c].track = NULL;
		channels[c].trackPtr = 0;
		channels[c].trackLength = 0;
		channels[c].wtLength = 16;
		channels[c].cs = STOPPED;
		channels[c].volume = 16;
	}

	pwmSlice = pwm_gpio_to_slice_num(pwmGpio);

	// configure PWM slice to output "0V"
	// this seems to reduce sound quality a bit - although it's still far from good
//	pwm_set_clkdiv_int_frac(pwmSlice, 3, 0);
	pwm_set_wrap(pwmSlice, pwmMax);
	pwm_set_counter(pwmSlice, 0);
	pwm_set_both_levels(pwmSlice, pwmCenter, pwmCenter);

	// initialize GPIO pin for current slice
	gpio_init(pwmGpio);
	gpio_set_dir(pwmGpio, GPIO_OUT);
	gpio_set_function(pwmGpio, GPIO_FUNC_PWM);

	// now configure DMA. First select an idle channel.
	dmaChannel = dma_claim_unused_channel(true);

	// Next create the configuration.
	dmaChannelConf = dma_channel_get_default_config(dmaChannel);
	// we need 16 bit transfers (the PWM compare register is 16 Bits wide)
	channel_config_set_transfer_data_size(&dmaChannelConf, DMA_SIZE_16);
	// read address needs to be incremented as we're reading from a buffer
	channel_config_set_read_increment(&dmaChannelConf, true);
	// write address must be constant when writing to PWM compare register
	channel_config_set_write_increment(&dmaChannelConf, false);

	// get a DMA timer and configure it to push out samples at the defined sample rate	
	dmaTimer = dma_claim_unused_timer(true);
	dma_timer_set_fraction(dmaTimer, DMA_CLOCK_DIVIDER_X, DMA_CLOCK_DIVIDER_Y);
	// set timer as data request source
	channel_config_set_dreq(&dmaChannelConf, dma_get_timer_dreq(dmaTimer));
	
	// configure the DMA channel; write address can be set up statically, write count varies
	dma_channel_configure(
		dmaChannel,
		&dmaChannelConf,
		&pwm_hw->slice[pwmSlice].cc,	// write address (counter compare register of selected PWM slice)
		NULL,			// no read address configured yet
		32,			// each of our buffers contains 32 items
		false			// don't start
	);

	// enable IRQ0 for the channel
	dma_channel_set_irq0_enabled(dmaChannel, true);

	// set the interrupt handler and enable the interrupt
	irq_set_exclusive_handler(DMA_IRQ_0, nextBufferInterrupt);
	irq_set_enabled(DMA_IRQ_0, true);

	pwm_set_enabled(pwmSlice, true);

	initialized = true;
}

void setBeat(uint bpm, enum Subbeat sb) {
	uint activationsPerMinute;

	if(!initialized)
		return;

	switch(sb) {
	case FULL:
		activationsPerMinute = 1;
		break;
	case HALF:
		activationsPerMinute = 2;
		break;
	case QUARTER:
		activationsPerMinute = 4;
		break;
	case EIGTH:
		activationsPerMinute = 8;
		break;
	case SIXTEENTH:
		activationsPerMinute = 16;
		break;
	}

	activationsPerMinute *= bpm;

	if((bpm > 5) && (bpm < 200)) {
		if((bpm != sndBpm) || (sb != subbeat)) {
			sndBpm = bpm;
			subbeat = sb;

			trackPtrUpdateDeltaSamples = ((SAMPLES_PER_MINUTE / activationsPerMinute) << 8) + (((SAMPLES_PER_MINUTE % activationsPerMinute) << 8) / activationsPerMinute);

			printf("Track pointer updates after %u.(%u/256) samples\n", trackPtrUpdateDeltaSamples >> 8, trackPtrUpdateDeltaSamples & 0xff);
		}

		if(!running) {
			// if sound isn't running right now, start it.
			startSound();
		}
	}
}

int setTrack(uint channel, uint trackLen, uint8_t *track) {
	// NOTE: upper limit on track length is rather randomly chosen.
	if((!initialized) || (channel >= N_SOUND_CHANNELS) || (trackLen == 0) || (trackLen > 0x10000) || (track == NULL))
		return -1;

	channels[channel].trackLength = trackLen;
	channels[channel].track = track;

	// this notifies the playback code that no note has been played before
	channels[channel].trackPtr = 0;

	printf("channel %u: setTrack okay, length: %u\n", channel, trackLen);

	return 0;
}


int playChannel(uint channel) {
	// check correct configuration first
	if((!initialized) || (channel >= N_SOUND_CHANNELS))
		return -1;

	// reset the track pointer if the channel has been stopped before
	if(channels[channel].cs == STOPPED) {
		channels[channel].trackPtr = 0;
	}

	channels[channel].cs = PLAYING;

	printf("channel %u started\n", channel);

	return 0;
}

int pauseChannel(uint channel) {
	if((!initialized) || (channel >= N_SOUND_CHANNELS))
		return -1;

	if(channels[channel].cs == PLAYING)
		channels[channel].cs = PAUSED;

	return 0;
}

void stopChannel(uint channel) {
	if((!initialized) || (channel >= N_SOUND_CHANNELS))
		return;

	channels[channel].trackPtr = 0xffffffff;

	channels[channel].cs = STOPPED;
}

void setChannelVolume(uint channel, uint vol) {
	if((!initialized) || (channel >= N_SOUND_CHANNELS) || (vol > 15))
		return;

	channels[channel].volume = vol + 1;
}

void setWaveform(uint channel, enum Waveform wf) {
	if((!initialized) || (channel >= N_SOUND_CHANNELS))
		return;

	if(wf == PCM)
		return;

	channels[channel].wf = wf;
	channels[channel].wtLength = 16;

	switch(wf) {
	case SINE:
		channels[channel].wavetable = sine;
		break;
	case TRIANGLE:
		channels[channel].wavetable = triangle;
		break;
	case SAWTOOTH:
	case SQUARE:
	case NOISE:
		break;
	default:
		break;	
	}
}

void setCustomWaveform(uint channel, uint wtLength, uint16_t *wf) {
	if((!initialized) || (channel >= N_SOUND_CHANNELS))
		return;

	channels[channel].wf = PCM;
	// TBD
}