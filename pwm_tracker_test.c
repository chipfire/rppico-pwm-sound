#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "hardware/dma.h"
#include "hardware/pll.h"
#include "hardware/clocks.h"

#include "pwm_retrosound.h"

uint8_t track[2][84] = {{
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(D, 3),
			MAKE_NOTE(D, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(F, 3),
			MAKE_NOTE(F, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(A, 4),
			PAUSE,
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(G, 3),
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(F, 3),
			PAUSE,
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			PAUSE,
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			MAKE_NOTE(E, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(G, 3),
			PAUSE,
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			MAKE_NOTE(C, 3),
			PAUSE,
			PAUSE,
			PAUSE,
			PAUSE},
			{MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			MAKE_NOTE(D, 1),
			MAKE_NOTE(D, 1),
			MAKE_NOTE(E, 1),
			MAKE_NOTE(E, 1),
			MAKE_NOTE(F, 1),
			MAKE_NOTE(F, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			PAUSE,
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			PAUSE,
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(A, 2),
			PAUSE,
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(G, 1),
			MAKE_NOTE(F, 1),
			PAUSE,
			MAKE_NOTE(F, 1),
			PAUSE,
			MAKE_NOTE(F, 1),
			PAUSE,
			MAKE_NOTE(F, 1),
			PAUSE,
			MAKE_NOTE(E, 1),
			MAKE_NOTE(E, 1),
			MAKE_NOTE(E, 1),
			PAUSE,
			MAKE_NOTE(E, 1),
			MAKE_NOTE(E, 1),
			MAKE_NOTE(E, 1),
			PAUSE,
			MAKE_NOTE(G, 1),
			PAUSE,
			MAKE_NOTE(G, 1),
			PAUSE,
			MAKE_NOTE(G, 1),
			PAUSE,
			MAKE_NOTE(G, 1),
			PAUSE,
			MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			MAKE_NOTE(C, 1),
			PAUSE,
			PAUSE,
			PAUSE,
			PAUSE}
};

uint8_t track2[] = {
	MAKE_NOTE(C, 1),
	MAKE_NOTE(C, 1),
	MAKE_NOTE(C, 1),
	MAKE_NOTE(C, 1),
	PAUSE,
	PAUSE,
	PAUSE,
	PAUSE,
	MAKE_NOTE(E, 1),
	MAKE_NOTE(E, 1),
	MAKE_NOTE(E, 1),
	MAKE_NOTE(E, 1),
	PAUSE,
	PAUSE,
	PAUSE,
	PAUSE
};

uint trackLen = 84, track2Len = 16;

int main() {
	set_sys_clock_khz(141600, true);

	stdio_init_all();

	sleep_ms(2000);

	printf("started\n");

	retrosound_init(0, 16);

	setBeat(120, QUARTER);

	setTrack(0, trackLen, track[0]);
//	setTrack(1, trackLen, track[1]);
	setTrack(1, track2Len, track2);

	setWaveform(0, SINE);
	setWaveform(1, TRIANGLE);

	setChannelVolume(0, 7);
	setChannelVolume(1, 7);

	playChannel(0);
	playChannel(1);

	while(1) {}

	return 0;
}