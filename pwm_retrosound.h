#include <stdint.h>

#ifndef PWM_RETROSOUND_H__
#define PWM_RETROSOUND_H__

#define A		0
#define B		1
#define H		2
#define C		3
#define Cis		4
#define D		5
#define Dis		6
#define E		7
#define F		8
#define Fis		9
#define G		10
#define Gis		11

#define MAKE_NOTE(t, o)		(((((o) & 0x7) << 4) | ((t) & 0xf)))
//#define MAKE_NOTE(t, o)		((uint8_t)((((o) & 0x7) << 4) | ((t) & 0xf)))

#define PAUSE		0xff

#define N_SOUND_CHANNELS	4

enum Subbeat {FULL, HALF, QUARTER, EIGTH, SIXTEENTH};

enum Waveform {SINE, TRIANGLE, SAWTOOTH, SQUARE, NOISE, PCM};

enum ChannelStatus {STOPPED, PLAYING, PAUSED};

void retrosound_init(uint timerAlarm, uint pwmGpio);

void setBeat(uint bpm, enum Subbeat sb);

int setTrack(uint channel, uint trackLen, uint8_t *track);

int playChannel(uint channel);

int pauseChannel(uint channel);

void stopChannel(uint channel);

void setChannelVolume(uint channel, uint vol);

void setWaveform(uint channel, enum Waveform wf);

// Can be used to add a custom waveform by supplying a list of samples.
// The waveform must contain a power of two number of samples, otherwise
// DMA doesn't work.
void setCustomWaveform(uint channel, uint wtLength, uint16_t *wf);

#endif