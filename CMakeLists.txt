# this is from pico-examples' root directory
cmake_minimum_required(VERSION 3.12)

message($ENV{PICO_SDK_PATH})

# Pull in SDK (must be before project)
#include($ENV{PICO_SDK_PATH}/external/pico_sdk_import.cmake)
include(pico_sdk_import.cmake)

message($ENV{PICO_SDK_PATH})

project(pwm_snd_test C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

if (PICO_SDK_VERSION_STRING VERSION_LESS "1.3.0")
    message(FATAL_ERROR "Raspberry Pi Pico SDK version 1.3.0 (or later) required. Your version is ${PICO_SDK_VERSION_STRING}")
endif()

# Initialize the SDK
pico_sdk_init()

add_compile_options(-Wall
        -Wno-format          # int != int32_t as far as the compiler is concerned because gcc has int32_t as long int
        -Wno-unused-function # we have some for the docs that aren't called
        -Wno-maybe-uninitialized
        )

# this is the source specific part merged from hello_pio and hello_dma

add_executable(pwm_snd_test)
add_executable(pwm_tracker_test)

#pico_generate_pio_header(paltest ${CMAKE_CURRENT_LIST_DIR}/PAL3.pio)

target_sources(pwm_snd_test PRIVATE pwm_snd_test.c)
target_sources(pwm_tracker_test PRIVATE pwm_tracker_test.c pwm_retrosound.c)

pico_enable_stdio_usb(pwm_snd_test 1)
pico_enable_stdio_usb(pwm_tracker_test 1)

target_link_libraries(pwm_snd_test PRIVATE
        pico_stdlib
        hardware_pwm
        hardware_dma
	hardware_timer
        )

target_link_libraries(pwm_tracker_test PRIVATE
        pico_stdlib
        hardware_pwm
        hardware_dma
	hardware_timer
        )

pico_add_extra_outputs(pwm_snd_test)
pico_add_extra_outputs(pwm_tracker_test)

# add url via pico_set_program_url
#example_auto_set_url(paltest)
