#include <stdint.h>

#ifndef PWM_RETROSOUND_CONSTS_H__
#define PWM_RETROSOUND_CONSTS_H__

#define ALARM_IRQ	0

#define MIXER_SAMPLE_RATE	48000
#define SAMPLES_PER_MINUTE	MIXER_SAMPLE_RATE*60

// dividing the 141.6 MHz clock by 2950 gives exactly 48 kHz
#define DMA_CLOCK_DIVIDER_X	1
#define DMA_CLOCK_DIVIDER_Y	2950

const uint16_t pwmMax = 1023;
const uint16_t pwmCenter = (pwmMax >> 1) + 1;

//int16_t sine[] = {0, 48, 90, 117, 127, 117, 90, 48, 0, -49, -91, -118, -128, -118, -91, -49};
//int16_t triangle[] = {0, 31, 63, 95, 127, 95, 63, 31, 0, -32, -64, -96, -128, -96, -64, -32};

int16_t sine[] = {0, 195, 361, 472, 511, 472, 361, 195, 0, -196, -362, -473, -512, -473, -362, -196};
int16_t triangle[] = {0, 127, 255, 383, 511, 383, 255, 127, 0, -128, -256, -384, -512, -384, -256, -128};

// 16.16 fixed point values to determine the next sample to be mixed
const uint toneStepPerSample[] = {
				601, 636, 675, 714, 756, 802, 850, 900, 955, 1009, 1070, 1134, 0, 0, 0, 0,
				1201, 1274, 1348, 1429, 1514, 1603, 1700, 1800, 1907, 2021, 2141, 2268, 0, 0, 0, 0,
				2403, 2545, 2698, 2857, 3028, 3207, 3399, 3600, 3814, 4041, 4282, 4537, 0, 0, 0, 0,
				4806, 5092, 5394, 5715, 6056, 6415, 6797, 7201, 7629, 8083, 8563, 9072, 0, 0, 0, 0,
				9612, 10183, 10789, 11431, 12110, 12830, 13593, 14402, 15258, 16165, 17127, 18145, 0, 0, 0, 0,
				19224, 20367, 21578, 22861, 24221, 25661, 27187, 28803, 30516, 32331, 34253, 36290, 0, 0, 0, 0,
				38448, 40734, 43156, 45722, 48441, 51322, 54373, 57607, 61032, 64661, 68506, 72580, 0, 0, 0, 0,
				76896, 81468, 86312, 91445, 96882, 102643, 108747, 115212, 122063, 129322, 137012, 145160, 0, 0, 0, 0,
				153791, 162936, 172624, 182889, 193764, 205287, 217494, 230427, 244128, 258644, 274023, 290318, 307582, 0, 0, 0, 0
};


typedef struct {
	enum Waveform wf;
	int16_t* wavetable;
	uint8_t* track;
	uint trackPtr, trackLength, wtLength;
	enum ChannelStatus cs;
	uint volume;
	uint sampleOffset;
} channel_t;


#endif